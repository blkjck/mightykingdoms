###Mighty Kingdoms

####Summer - repeat for 6 months
||Phase|Effect|
|:--|:-----------------|:-------------------------|
|A|Orders|Move, Defend, Raze, Regroup|
|B|Battle|Where more than one Kingdom has armies in a tile a battle occurs|
|C|Resolve|Lay down armies that crossed terrain. Take control where one Kingdom in tile. Set new player order| 

#####Exploration table
|2D6|Tile contents|
|:--------|:--------------|
|2-7|Empty|
|8-9|Village|
|10|City|
|11-12|Fortress|
<br>  

-------

<br>  
<br>

###Mighty Kingdoms

####Winter

||Phase|Effect|
|:--|:-----------------|:-------------------------|
|A|Recall armies|Trace line of supply back to Capital or be destroyed|
|B|Gather resources|Villages = Food, Cities = Gold|
|C|Spend resources|Tithe: Give away a resource. Build settlements. Recruit armies - 1 per Fortress. Feed armies - 1 food each|
|D|Deploy armies|Placed in settlements connected to Capital. Max: 1 per Village, 2 per City, 3 per Fortress|
